import sqlite from 'sqlite'

export default function getUsers(req, res) {
  if (req.method !== 'GET') {
    res.status(500).json({ message: 'Sorry only accept GET request' })
  }
  res.json({ text: 'Hello', method: req.method })
}
